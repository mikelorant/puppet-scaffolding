class role::generic {
  notify { 'I have no specific role': }

  package { 'awscli': }
  package { 'crudini': }
  package { 'git': }
}
