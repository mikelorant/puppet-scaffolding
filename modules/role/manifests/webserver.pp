class role::webserver {
  notify { 'I am a webserver': }

  $site = "www.${::domain}"

  class { 'apache': }

  apache::vhost { $site:
    docroot => '/var/www/html'
  }
}
