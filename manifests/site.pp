node default {
  case $::cfn_stack_name {
    /^(infrastructure-puppetmaster)/: { include role::puppetmaster }
    /^(infrastructure-webserver)/:    { include role::webserver }
    default:                          { include role::generic }
  }  
}
